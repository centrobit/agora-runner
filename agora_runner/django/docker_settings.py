DATABASES['default'].update({
    'PASSWORD': '12345',
    'HOST': 'agora-postgres',
    'PORT': '5432',
    'NAME': 'agora',
})

COMPRESS_ENABLED = False
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_DOMAIN = None

TEMPLATE_LOADERS = (
    'templateloaderwithpriorities.Loader',
    'django.template.loaders.filesystem.Loader',
    'dbtemplates.loader.Loader',
    'django.template.loaders.app_directories.Loader',
)

CACHES['default']['LOCATION'] = 'redis://agora-redis:6379/1'
CACHES['core-cache']['LOCATION'] = 'redis://agora-redis:6379/1'
CACHES['contractor-cache']['LOCATION'] = 'redis://agora-redis:6379/1'

BROKER_URL = 'redis://agora-redis:6379/1'
CELERY_RESULT_BACKEND = 'redis://agora-redis:6379/1'

CACHEOPS_REDIS = {
    'host': 'agora-redis',
    'port': 6379,
    'db': 1,
}

LOGGING['handlers']['logfile']['filename'] = os.path.join(BASE_DIR, 'agora.web.log')

if 'logfile_debug' in LOGGING['handlers']:
    LOGGING['handlers']['logfile_debug']['filename'] = os.path.join(BASE_DIR, 'agora.web.log')

DEBUG = True
