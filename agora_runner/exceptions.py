# -*- coding: utf-8 -*-


class CommandError(Exception):
    """
    Если этот эксепшн будет брошен внутри блока run, то он будет обработан,
    и вместо исключения пользователю будет отображено сообщение об ошибке.
    """
    pass


class FTPLoginIncorrect(Exception):
    pass
