# -*- coding: utf-8 -*-
import ftplib
import os
import re
import datetime
import subprocess
import requests
import base64
import time
import gzip
import shutil
import configparser

from os.path import dirname, join
from requests.compat import urljoin

from agora_runner import conf
from agora_runner.exceptions import CommandError, FTPLoginIncorrect


def clean_pyc():
    for root, subdirs, files in os.walk(os.getcwd()):
        for file in files:
            if re.search(r'\.pyc$', file) is not None:
                os.remove(os.path.join(root, file))


def create_env(project_name, db_dump):
    if not db_dump:
        raise CommandError("Dump for {} not found.".format(project_name))

    if os.path.exists('.env'):
        os.remove('.env')

    if not os.path.exists('.env.example'):
        raise CommandError("File .env.example not found")

    with open('.env.example', 'r') as f:
        env = f.read()

    db_dump_path = os.path.join(os.getcwd(), conf.DUMP_DIR)

    if os.name == 'nt':
        '''
            re.sub криво работает с путями windows. Конверитруем бекслеши в обычные.
        '''
        db_dump_path = os.path.join(os.getcwd(), conf.DUMP_DIR).replace('\\', '/')

    env = re.sub(r'PROJECT_NAME=', 'PROJECT_NAME={}'.format(project_name), env)
    env = re.sub(r'DB_DUMP=', 'DB_DUMP={}'.format(db_dump), env)
    env = re.sub(r'DUMP_DIR=.*\n', 'DUMP_DIR={}\n'.format(db_dump_path), env)

    with open('.env', 'w') as f:
        f.write(env)
        f.write('\nDJANGO_SETTINGS_MODULE=agora.settings.{}'.format(project_name))


def get_db_dump_from_dir(project_name):
    db_dump = None
    res = []
    dumps_dir = os.path.join(os.getcwd(), conf.DUMP_DIR)
    os.makedirs(dumps_dir, exist_ok=True)
    dumps = os.listdir(dumps_dir)

    # Выполняем поиск дампов в локальной директории с дампами

    for dump in dumps:
        if re.search(r'.*{}.*'.format(project_name), dump) is not None:
            res.append(dump)

    if len(res) > 1:
        last_date = datetime.datetime.fromtimestamp(
            os.stat(os.path.join(os.path.join(os.getcwd(), conf.DUMP_DIR), res[0])).st_mtime)

        db_dump = res[0]
        for dump in res:
            dump_date = datetime.datetime.fromtimestamp(
                os.stat(os.path.join(os.path.join(os.getcwd(), conf.DUMP_DIR), dump)).st_mtime)
            if (dump_date - last_date).total_seconds() > 0:
                db_dump = dump

    elif len(res) == 1:
        db_dump = res[0]

    return db_dump


def get_db_dump_from_ftp(project_name):
    """
    Функция возращает имя дампа БД, для того чтобы добавить его в .evn файл.
    Сначала выполняется поиск дампа БД в директории с дампами, если он не будет
    найден, пробуем автоматически загрузить его с ftp сервера через запрос в jenkins.

    :param project_name:
    :return:
    """
    config = configparser.ConfigParser(interpolation=None)
    config.read(conf.CONFIG_RUNNER_FILENAME)

    ftp_reqs = {
        'ip': config.get('ftp', 'ip', fallback=None),
        'user': config.get('ftp', 'user', fallback=None),
        'password': config.get('ftp', 'password', fallback=None),
    }

    jenkins_reqs = {
        'address': config.get('jenkins', 'address', fallback=None),
        'user': config.get('jenkins', 'user', fallback=None),
        'password': config.get('jenkins', 'password', fallback=None),
    }

    data_reqs = (
        ('jenkins', jenkins_reqs),
        ('ftp', ftp_reqs),
    )

    for service, data in data_reqs:
        if not all(data.values()):
            raise CommandError("You should specify {} connection data in your config {}.".format(
                service,
                conf.CONFIG_RUNNER_FILENAME,
            ))

    # Если нет ни одного, то пробуем загрузить с ftp, для этого отправляем запрос в дженкинс,
    # и ожидаем пока дамп будет подготовлен к выгрузке

    jenkins_url_get_dump = '{}view/pipeline/job/admin%20agora%20api/buildWithParameters?token=agora&job={}&project={}'.format(
        jenkins_reqs['address'],
        conf.JENKINS_DUMP_JOB,
        project_name)

    dump_filename = conf.DUMP_FILENAME_MASK.format(project_name)

    response = requests.post(
        jenkins_url_get_dump,
        auth=(jenkins_reqs['user'], jenkins_reqs['password']),
    )

    if response.status_code == 201:
        ftp = ftplib.FTP(ftp_reqs['ip'])

        try:
            ftp.login(ftp_reqs['user'], ftp_reqs['password'])
        except ftplib.error_perm as err:
            if str(err)[:3] == '530':
                raise FTPLoginIncorrect()
            else:
                raise err

        attempts = 300

        while attempts > 0:
            try:
                ftp.retrbinary(
                    'RETR {}'.format(dump_filename),
                    open(os.path.join(conf.DUMP_DIR, dump_filename), 'wb').write
                )
            except Exception:
                time.sleep(1)
                attempts -= 1
            else:
                break

        if attempts == 0:
            raise CommandError("Error downloading dump from ftp, check jenkins queue and ftp access.")
    else:
        raise CommandError("Error sending request to jenkins: {}.".format(response.status_code))

    with gzip.open(os.path.join(conf.DUMP_DIR, dump_filename), 'rb') as f_in:
        with open(os.path.join(conf.DUMP_DIR, dump_filename[:-3]), 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    os.remove(os.path.join(conf.DUMP_DIR, dump_filename))

    db_dump = dump_filename[:-3]

    return db_dump


def get_settings(project_name, uri, auth):
    s = requests.get(uri, headers={'Authorization': 'Basic {0}'.format(auth)})
    if s.status_code == 401:
        raise CommandError("Bitbucket auth fail.")

    for p in s.json()['values']:
        if p['path'] == project_name:
            s = requests.get(
                urljoin(p['links']['self']['href'], 'local.py'),
                headers={'Authorization': 'Basic {0}'.format(auth)},
            )
            return s.content.decode('utf-8')

    if 'next' in s.json().keys():
        s = get_settings(project_name=project_name, uri=s.json()['next'], auth=auth)
        return s
    else:
        raise CommandError("Bitbucket error.")


def create_settings(project_name):
    user_config = configparser.ConfigParser(interpolation=None)
    user_config.read(conf.CONFIG_RUNNER_FILENAME)

    auth = (
        user_config.get('bitbucket', 'user', fallback=None),
        user_config.get('bitbucket', 'password', fallback=None),
    )

    if not all(auth):
        raise CommandError("You should specify bitbucket auth in your config {}".format(conf.CONFIG_RUNNER_FILENAME))

    auth_payload = base64.b64encode(('{}:{}'.format(*auth)).encode('ascii')).decode()

    project_setting = get_settings(
        project_name=project_name,
        uri=conf.SETTINGS_REPO_URI,
        auth=auth_payload,
    )

    with open(join(dirname(__file__), 'django/docker_settings.py'), 'r') as docker_settings:
        with open(os.path.join(os.getcwd(), conf.SETTINGS_DIR, '{}.py'.format(project_name)), 'w') as f:
            f.write(project_setting)
            f.write('\n')
            f.write(docker_settings.read())


def docker_up():
    proc = subprocess.Popen(['docker-compose', 'up', '-d'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    success = len(re.findall(b'done', out + err)) >= 3
    return success, out + err


def docker_down():
    proc = subprocess.Popen(['docker-compose', 'down'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    success = len(re.findall(b'done', out + err)) >= 3
    return success, out + err


def get_ready_postgres():
    time.sleep(10)

    while True:
        proc = subprocess.Popen(['docker', 'logs', 'agora-postgres', '--tail=10'], stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        o, e = proc.communicate()

        if b'database system is ready to accept connections' in (o + e):
            break
        time.sleep(5)


def get_ready_web():
    time.sleep(10)

    while True:
        proc = subprocess.Popen(['docker', 'logs', 'agora-web'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        o, e = proc.communicate()

        if b'Starting development server' in (e + o):
            return
        time.sleep(5)


def color_green(text):
    return '\x1b[38;5;2m{}\x1b[0m'.format(text)


def color_red(text):
    return '\x1b[38;5;1m{}\x1b[0m'.format(text)


def create_default_agorarc():
    config = configparser.ConfigParser(interpolation=None)

    config['bitbucket'] = {
        'user': 'user',
        'password': 'password',
    }
    config['ftp'] = {
        'ip': '0.0.0.0',
        'user': 'user',
        'password': 'password',
    }
    config['jenkins'] = {
        'address': 'http://jenkins.centrobit.ru/',
        'user': 'user',
        'password': 'password',
    }

    with open(conf.CONFIG_RUNNER_FILENAME, 'w') as configfile:
        config.write(configfile)
