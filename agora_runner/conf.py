# -*- coding: utf-8 -*-
import os

SETTINGS_REPO = 'agora-settings'
SETTINGS_REPO_URI = 'https://api.bitbucket.org/2.0/repositories/centrobit/agora-settings/src'
SETTINGS_DIR = os.path.join('agora', 'settings')

CONFIG_RUNNER_FILENAME = '.agorarc'

DUMP_DIR = os.path.join('dumps')
DUMP_FILENAME_MASK = 'optima3_{}.sql.gz'

JENKINS_DUMP_JOB = 'Свежий%20дамп'
