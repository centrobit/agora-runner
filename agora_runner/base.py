# -*- coding: utf-8 -*-
import argparse
import sys


class OutputWrapper(object):
    """
    Враппер stdout/stderr
    """
    def __init__(self, out, ending='\n'):
        self._out = out
        self.ending = ending

    def __getattr__(self, name):
        return getattr(self._out, name)

    def isatty(self):
        return hasattr(self._out, 'isatty') and self._out.isatty()

    def write(self, msg, ending=None):
        ending = self.ending if ending is None else ending
        if ending and not msg.endswith(ending):
            msg += ending
        self._out.write(msg)


class BaseCommand:
    def __init__(self):
        self.stdout = OutputWrapper(sys.stdout)
        self.stderr = OutputWrapper(sys.stderr)

    def execute(self, argv):
        parser = argparse.ArgumentParser(
            prog='agora-runner',
            description='Tool for easy run agora projects',
        )

        self.add_arguments(parser)

        command_options = vars(parser.parse_args(argv))

        self.handle(**command_options)

    def add_arguments(self, parser):
        pass

    def handle(self, **options):
        raise NotImplementedError('subclasses of BaseCommand must provide a handle() method')
