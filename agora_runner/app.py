# -*- coding: utf-8 -*-
import os
import sys

from os.path import dirname, join
from importlib import import_module
from agora_runner.exceptions import CommandError


COMMANDS_DIR = ['agora_runner', 'commands']


def fetch_commands():
    return {
        _file[:-3]: _file
        for _file in os.listdir(join(dirname(dirname(__file__)), *COMMANDS_DIR))
        if all([
            _file.endswith('.py'),
            _file != '__init__.py',
        ])
    }


def is_current_dir_agora():
    files_in_current_dir = os.listdir(os.getcwd())

    return all([
        'agora' in files_in_current_dir,
        'manage.py' in files_in_current_dir,
    ])


def run():
    if len(sys.argv) < 2:
        sys.stdout.write("Available commands:\n")

        for command, file_name in fetch_commands().items():
            sys.stdout.write("\t{}\n".format(command))

        sys.exit(1)

    command_name = sys.argv[1]

    try:
        command_module = import_module('{}.{}'.format('.'.join(COMMANDS_DIR), command_name))
    except ImportError:
        sys.stdout.write("Unknown command: {}\n".format(command_name))
        sys.exit(1)

    if not is_current_dir_agora():
        sys.stdout.write("You should be in agora root directory.\n")
        sys.exit(1)

    command_class = command_module.CommandClass()

    try:
        command_class.execute(sys.argv[2:])
    except Exception as e:
        if not isinstance(e, CommandError):
            raise

        sys.stderr.write('\n%s: %s' % (e.__class__.__name__, e))
        sys.exit(1)


if __name__ == '__main__':
    run()
