# -*- coding: utf-8 -*-
import os
import sys
import docker

from os.path import join
from dotenv import dotenv_values

from agora_runner import conf
from agora_runner import utils
from agora_runner.utils import color_green, color_red
from agora_runner.base import BaseCommand
from agora_runner.exceptions import CommandError, FTPLoginIncorrect


class CommandClass(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            'project',
            nargs=1,
            help='Name of project.',
        )

        parser.add_argument(
            '--update-conf',
            dest='update_conf',
            action='store_true',
            help='Update settings file.',
        )

        parser.add_argument(
            '--update-dump',
            dest='update_dump',
            action='store_true',
            help='Update dump.',
        )

        parser.add_argument(
            '--update',
            dest='update',
            action='store_true',
            help='Update dump and settings.',
        )

    def handle(self, **options):
        if not options.get('project'):
            raise CommandError("You should specify a name of project.")

        project_name = options.get('project')[0]
        update_dump = options.get('update_dump') or options.get('update')
        update_conf = options.get('update_conf') or options.get('update')

        client = docker.from_env()

        try:
            container_agora = client.containers.get('agora-web')
        except Exception:
            container_agora = None

        if container_agora and container_agora.status == 'running':
            env_values = dotenv_values(dotenv_path=join(os.getcwd(), '.env'))
            self.stdout.write(f"Development server already running")
            self.stdout.write(f" - config: agora.settings.{env_values.get('PROJECT_NAME', '')}")
            self.stdout.write(f" - dump: {join(conf.DUMP_DIR, env_values.get('DB_DUMP', ''))}")

            if env_values.get('PROJECT_NAME', '') != project_name:
                self.stdout.write(
                    f"\nIf you want up \"{project_name}\" you should down current project. "
                    f"Use command \"agora down\" or \"docker-compose down\".")

            sys.exit(1)

        if not os.path.exists(conf.CONFIG_RUNNER_FILENAME):
            self.stdout.write(f"To run command you should specify config {conf.CONFIG_RUNNER_FILENAME}.")
            self.stdout.write(f"-> Create default config {conf.CONFIG_RUNNER_FILENAME} ... ", ending='')
            self.stdout.flush()
            utils.create_default_agorarc()
            self.stdout.write(color_green('done'))
            self.stdout.write(f"\nConfig was created, open file {conf.CONFIG_RUNNER_FILENAME} in editor and fill all fields.")
            sys.exit(1)

        self.stdout.write("-> Clean python compiled files ... ", ending='')
        self.stdout.flush()
        utils.clean_pyc()
        self.stdout.write(color_green('done'))

        db_dump = utils.get_db_dump_from_dir(project_name)

        self.stdout.write("-> Download DB dump ... ".format(project_name), ending='')
        self.stdout.flush()
        if not db_dump or update_dump:
            try:
                db_dump = utils.get_db_dump_from_ftp(project_name)
            except FTPLoginIncorrect:
                self.stdout.write(color_red('fail'))
                raise CommandError("FTP login or password incorrect, update your .agorarc config.\n You can find the current password here https://confluence.centrobit.ru/display/SUPPORT/FTP+agora")

        self.stdout.write(color_green('done'))

        self.stdout.write("-> Create .env file ... ", ending='')
        self.stdout.flush()
        utils.create_env(project_name, db_dump)
        self.stdout.write(color_green('done'))

        self.stdout.write("-> Download settings ... ", ending='')
        self.stdout.flush()
        if any([
            not os.path.exists(join(os.getcwd(), conf.SETTINGS_DIR, project_name + '.py')),
            update_conf,
        ]):
            utils.create_settings(project_name)
        self.stdout.write(color_green('done'))

        self.stdout.write("-> Docker up ... ", ending='')
        self.stdout.flush()
        success, out = utils.docker_up()

        if not success:
            self.stdout.write(color_red('fail'))
            raise CommandError("Docker is not running, output: \n\n" + out.decode('utf-8'))

        self.stdout.write(color_green('done'))

        self.stdout.write("-> Restore dump ... ", ending='')
        self.stdout.flush()
        utils.get_ready_postgres()
        self.stdout.write(color_green('done'))

        self.stdout.write("-> Start django ... ", ending='')
        self.stdout.flush()
        utils.get_ready_web()
        self.stdout.write(color_green('done'))

        self.stdout.write("\nStarting development server at http://0:8000/")
        self.stdout.write(f" - config: agora.settings.{project_name}")
        self.stdout.write(f" - dump: {join(conf.DUMP_DIR, db_dump)}")
