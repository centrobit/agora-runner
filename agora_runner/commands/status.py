# -*- coding: utf-8 -*-
import os
import sys
import docker

from os.path import join
from dotenv import dotenv_values

from agora_runner import conf
from agora_runner.base import BaseCommand


class CommandClass(BaseCommand):
    def handle(self, **options):
        client = docker.from_env()

        try:
            container_agora = client.containers.get('agora-web')
        except Exception:
            container_agora = None

        if not os.path.exists(join(os.getcwd(), '.env')):
            self.stdout.write(f"Development server status:")
            self.stdout.write(f" - config: undefined")
            self.stdout.write(f" - dump: undefined")
            self.stdout.write(f" - agora-web: down")
            sys.exit(1)

        env_values = dotenv_values(dotenv_path=join(os.getcwd(), '.env'))
        self.stdout.write(f"Development server status:")
        self.stdout.write(f" - config: agora.settings.{env_values.get('PROJECT_NAME', '')}")
        self.stdout.write(f" - dump: {join(conf.DUMP_DIR, env_values.get('DB_DUMP', ''))}")

        if container_agora:
            self.stdout.write(f" - agora-web: {container_agora.status}")
        else:
            self.stdout.write(f" - agora-web: down")
