# -*- coding: utf-8 -*-
import docker

from agora_runner import utils
from agora_runner.utils import color_green
from agora_runner.base import BaseCommand


class CommandClass(BaseCommand):
    def handle(self, **options):
        client = docker.from_env()

        try:
            container_agora = client.containers.get('agora-web')
        except Exception:
            container_agora = None

        self.stdout.write("-> Docker down ... ", ending='')
        self.stdout.flush()
        if container_agora:
            utils.docker_down()
        self.stdout.write(color_green('done'))
