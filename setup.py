import setuptools

install_requires = [
    'requests',
    'docker',
    'python-dotenv',
]

setuptools.setup(
    name="agora-runner",
    version="0.0.3",
    author="Konstantin Liuterovich",
    author_email="konstantinliuterovich@gmail.com",
    description="Runner for agora projects",
    long_description='',
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/centrobit/agora-runner",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    entry_points={
        'console_scripts': [
            'agora=agora_runner.app:run',
        ],
    },
    install_requires=install_requires,
)
