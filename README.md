# Agora runner

���������� python ���������� ��� �������� � �������� ������� �������� agora. �������������� ����� 
�������� ��� �������� ����� �� ��� �������, �������� ������� � ��������� .env ����� 
��� docker-compose. ��� ��� ���������� �������� ���� �������� � ������� ������ ���� ���������� docker. 

���������� �������� ������ ��� OC Linux � Mac OS. 

## �����������

- docker >= 19.03.9
- python >= 3.5
   
## ���������

���������� ��������� ����� pip ������. ����� ���������� ��� � ���������� ��������� 
�������� �������:

```
python3 -m pip install -e git+https://bitbucket.org/centrobit/agora-runner.git#egg=agora-runner --upgrade
```

���� ����� ��������� ������� ����� �� ��������, �� ����� ����������� ���������� �� sudo:

```
sudo python3 -m pip install -e git+https://bitbucket.org/centrobit/agora-runner.git#egg=agora-runner --upgrade
```

## ��������� 

����� �������� ���������, ���������� ����������� �������� "agora", ��� ����� 
����� ��������� � �������� ���������� �������, ��� ��� ��������� ���� manage.py

� ����� ������� ����� ������� ���� �������� ��� ����������, � ������� ����� 
��������� ��������� ������� � bitbucket, jenkins � ftp ��������. 

```
touch .agorarc
nano .agorarc
```

�������� ���� ���������� �� ����� �������:

```
[bitbucket]
user = user
password = pass

[ftp]
ip = 82.202.247.250
user = user
password = pass

[jenkins]
address = https://jenkins.agora.ru/
user = user
password = pass
```

� ����� user � password ����� ������� ��� ������������ � ������ ��� ������� � �������. ��� ��������� ������� ��� � ������ ������ ������������. 
��� ftp � jenkins ���������� ��������� ��������� �����: 

- https://confluence.centrobit.ru/display/SUPPORT/FTP+agora
- https://confluence.centrobit.ru/display/SUPPORT/Jenkins+API

## �������������

��� ������� ����� ������������ ������� ������� ������� support 

```
agora up support
```

����� ����� �������, �� ������� ��������� ����� � �������: 

```
-> Clean python compiled files ... done
-> Download DB dump ... done
-> Create .env file ... done
-> Download settings ... done
-> Docker up ... done
-> Restore dump ... done
-> Start django ... done

Starting development server at http://0:8000/
 - config: agora.settings.support
 - dump: dumps/optima3_support.sql
```

����� ����� ������ ����� �������� �� ������ http://0:8000/ ��� ������� 
������� ������ ��������� ������� � ���������� dumps/ �������� ����� ��
� �������� ������� � ���������� agora/settings/ ���� ���� � ��������� ���
���������, �� �������� ��� �� �����������. ��� ���� ����� ��������� �� 
������������� ����� �������� ���� --update

```
agora up support --update
```

����� ����� �������� ������������� ������ ������ ��� ������ ����, ��� ����� 
����� ������������ --update-conf ��� --update-dump ��������������.    

����� ���������� ������ ����� ������������ �������: 

```
agora down
```

��� �������, ������ ������� ��� �������� ������ docker-compose down.

����� ����, ��� ������ ������, ����� ������������ ����� ������� ������ ��� ������ ��� ��������.

��� ������� docker-compose ������������ ��� ����������: 

- agora-web 
- agora-postgres 
- agora-redis

�������������� ��� ���� ����� ��������� manage.py ������� django ����� ������������:

```
docker exec -it agora-web python ./manage.py shell_plus
docker exec -it agora-web python ./manage.py migrate 
docker exec -it agora-web python ./manage.py createsuperuser
```

� ������. ��� ������� manage.py ������� ��� ������������� ���������� �������� 
--settings ��� ��� ��� ������� ������� ������� ��������� ������������ ���������� 
��������� DJANGO_SETTINGS_MODULE � �������� �������.  

��� ��������� ���������� ����� ������ ������� ��� ��� ������� � ��������� ��� ����� 
������������ �������:

```
agora status
```

����� ����� ���������: 

```
Development server status:
 - config: agora.settings.support
 - dump: dumps/optima3_support.sql
 - agora-web: running
```

�� �������� ������� ��� � ������ ������ ������� ���-������ � 
�������� support � ����� ������ dumps/optima3_support.sql.


